---
title: Pourquoi les gens qui mangent de la viande sont des con·nes ? 
description: « Laisse moi manger mon steak de boeuf, sale vegan extrêmiste ! »
date: 2022-04-19
draft: false
comments: true
showToc: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowRssButtonInSectionTermList: true
tags: ["végétarien", "militantisme"]
---

J'ai conscience que le titre est peut-être un brin provocateur. La plupart d'entre vous doivent déjà se sentir agressé·e dès le début de cet article... Mais c'est pas grave, parfois il faut savoir se faire bousculer un peu ! Je pensais donner un titre un peu plus consensuel initialement, mais une conversation avec un ami carniste m'a fait un peu changer d'avis tellement les arguments lancés contre les végétarien·nes me semblaient soit stupide, soit d'une mauvaise foi rare.

> Tu manges pas de viande ni de poisson, mais tu manges des oeufs et du fromage ? Et tu penses à la souffrance des carottes aussi ? Puis faut bien manger quand même... En plus, c'est quand même vraiment trop bon la viande.

Mais que répondre devant cette accumulation d'argument globalement un peu pourri ? :D

J'ai envie de vous exposer comment je suis devenu végétarien et les raisons qui font que je le suis toujours et pourquoi c'est important d'opter pour une alimentation non-carné, au vu de ce qui se passe dans notre jolie monde.

Je n'ai pas envie d'avoir une posture moralisatrice, et vous faites bien ce que vous voulez de votre alimentation ! Pour autant, je pense qu'il est important de comprendre pourquoi de plus en plus de gens se tournent vers ce type d'alimentation et je regrette que la plupart des personnes qui continuent à manger de la viande décident tout simplement d'ignorer le problème pour des raisons de confort et de non remise en question, comme si tout ce qui en découle ne les concernaient pas.  C'est plutôt ça, qui m'agace un peu. (mais ça va j'vous aime quand même, un peu, bande de petit priviligié :D)

## Pourquoi et comment je suis devenu végétarien ?

Warning : cette partie est pas mal autobiographique, si tu veux pas que j'te raconte ma vie tu peux passer au chapitre suivant, promis je t'en voudrais pas <3

Bon, clairement, je suis pas devenu végétarien du jour au lendemain en claquant des doigts. Je pars déjà avec un avantage, j'ai jamais aimé la viande rouge et j'ai jamais de toute façon était un grand fan de viande. Je mangeais principalement de la viande blanche, et du poisson. Alors oui, parce que quand je dis que je suis devenu végétarien, c'est que je ne mange plus de **chair animal**, incluant le poisson, parce qu'aussi fou que cela puisse paraître mes chèr-es ami-es, le poisson est bien un être vivant ! Je précise quand même parce qu'on me pose systématiquement la question comme si le poisson était en fait... ben je sais pas, rien, une plante ? Un élément de décoration dans l'océan ? En tout cas moins important que les animaux terrestre.

Revenons à nos moutons. De mon côté, j'ai commencé à prendre conscience de cette question grâce à un camarade de classe que j'ai eu au début de mes études durant 2 ans. Lui était  à l'époque végétarien, et m'a convaincu petit à petit que manger de la viande était à la fois loin d'être indispensable, mais avait aussi un impact non-négligeable sur le monde. A la fin de ces 2 ans, je commençais à me dire que j'allais moi aussi devenir végétarien, mais je n'étais pas encore décidé à passer le cap. Il a fallu un mini éléctro-choc pour que je passe totalement le cap, et comme le monde est bien fait, il a pas fallu attendre très longtemps pour qu'il arrive.

L'été suivant la fin de mes deux premières années d'études, j'ai travaillé sur les marchés à vendre des légumes pour ma tante (expérience très sympa au passage), mais il s'avère que de ce côté de la famille, ça bouffe de la viande à gogo. Et quand je dis à gogo, c'est à tous les repas possible, la viande est au centre de tout. J'ai vécu une petite overdose qui m'a finalement convaincu qu'une fois que je repartirai de là (où j'ai passé 2 mois), j'arrêterai définitivement. Ce que j'ai fait, assez facilement. 

Aujourd'hui, cela doit faire 8 ans que je suis végétarien. Il m'est arrivé parfois accidentellement de remanger de la viande et du poisson, mais c'est quelque chose qui vraiment ne me donne plus du tout envie. Au final, on nous bassine depuis petit pour nous dire qu'il est important de manger de la viande tous les jours, un peu comme les produits laitiers, mais la réalité est assez différente. On se passe très bien de viande : pour rappel les humain-es sont des êtres omnivores et la consommation de viande est loin d'être indispensable à notre survie physique. Alors oui c'est vrai que j'ai quelques carences parce que je suis pas suuuuper assidu sur mon alimentation (bien que je commence à faire gaffe maintenant), mais en réalité, la plupart des gens carniste ou non ont des carences diverses, et tout ce dont vous avez besoin est trouvable ailleurs que dans la viande, et ça c'est quand même une bonne nouvelle. 

## Les 3 grandes raisons qui font que les gens arrêtent de manger de la viande

Ok, maintenant que j'ai raconté ma vie, on va peut-être s'arrêter sur les raisons qui peuvent pousser quelqu'un-e à arrêter de manger de la viande. De mon côté, j'ai trouvé trois arguments majoritaire qui pousse le monde à se passer d'un régime carniste.

### Pour ta propre santé

Eh ouais, pour ta santé ! Je commence par l'argument qui m'intéresse le moins mais bon, il reste important à aborder. Je dis ça parce que, c'est clairement pas celui qui m'a convaincu d'arrêter la viande. C'est difficile aujourd'hui d'avoir une vie 100% saine, et c'est de toute façon difficile de savoir qu'est ce qui est réellement bon pour le corps. Je pense surtout que l'important c'est d'éviter les abus et de trouver un équilibre qui nous correspond. 

[étude sur la santé]

### Pour la planète

Le second argument qui lui, a fait parti des arguments qui m'ont poussé à arrêter, c'est que la consommation de viande, ça pollue à donf, et c'est issue d'une industrie monstrueuse qui est franchement pas enviable dans le monde idéal qu'on veut tou-tes.

Je pense qu'il est pas forcément utile de faire un looong aparté sur la question du changement climatique, j'espère qu'on est tou-tes d'accord pour dire que globalement, c'est la merde, et que ça fait peur, et surtout que c'est vrai. Si vous doutez encore de ça, au mieux vous croyiez encore au père Noël, au pire vous êtes un climato-sceptique d'extrême droite et ça, ça fait peur. Parce que la communauté scientifique est tout de même maintenant unanime sur la question, ça fait plusieurs décennies qu'elle nous alerte sur les impacts et les danger de ne pas agir pour limiter ce dérègelement, et que globalement, tout le monde s'en fou. (quand je dis tout le monde, je vise surtout les gens qui ont du pouvoir, c-a-d nos politiques, les grosses entreprises et les riches, genre les gros gros richous)

Alors tu vas me dire...

> Jean-Michel Mauvaisefoi : Ouais mais face à ça, franchement, est-ce qu'arrêter de manger de la viande ça va vraiiiiment avoir un impact sur le monde ? J'suis pas sûre, j'préfère continuer à kiffer mes barbecue à base d'entrecôté fumé que d'arrêter moi...

J'vais pas te mentir, je crois pas particulièrement à l'impact qu'on peut avoir individuellement sur le monde donc j'vais pas te défendre cette thèse. Néanmois, il y a une question de cohérence idéologique important autour de cette enjeu, et même si cela reste un "geste individuel", il est l'un des plus simple et pertinent à réaliser si on prend en compte son impact.

- impact individuel vs le reste du monde (cela reste un très bon moyen d'agir)
- Etude scientifique sur l'impact de la viande pour le climat
- Une autre façon d'envisager de manger de la viande (En gros, qd y'a plus d'industrie de masse)

### Pour la vie animale

- Explication de ce que c'est l'antispécisme
- Pourquoi c'est intéressant (rapport aux autres changés, recherche de moins de souffrance, proche d'une forme d'anarchie idéologique)

