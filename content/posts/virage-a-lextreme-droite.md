---
title: Virage à (l'extrême) droite touuuuuute !
description: « Mobilisez-vous contre l'extrême gauche ! »
date: 2022-06-22
draft: true 
comments: true
showToc: true
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
ShowRssButtonInSectionTermList: true
tags: ["actualités", "politique"]
---

	Je veux éradiquer la colère dont le Front national se nourrit.

	Je combats l'extrême droite en France [...]

	Le parti de l'extrême droite est un parti qui ne ressemble pas à notre France.

Ben c'est raté. Toutes ces phrases sont des tweets de notre cher président Macron, datant de 2017 pendant la campagne présidentielle. Il est clair aujourd'hui que ces paroles résonnent vraiment étrangement. Aujourd'hui je suis en colère pour pas mal de raison. Parlons-en !

### La fin de la gauche et de la droite

Aujourd'hui le constat est assez clair : le clivage gauche/droite qu'on voit s'alterner depuis 30 ans au pouvoir est dépassé. Aujourd'hui il y a 3 blocs

- **La gauche radicale et écologiste** représenté notamment par Jean-Luc Mélenchon et de manière plus générale par la NUPES (Nouvelle Union Populaire Ecologique et Sociale, va trouver plus long je suis pas sûre que tu trouve) qui réunit notamment les socialistes, les communistes, les écologistes et les insoumis.
- **L'extrême droite** de Marine Lepen. Dédiabolisé de plus en plus comme si leurs idées avaient réellement évolué. Rendu plus "doux" au regard de tou-tes grâce notamment au traitement médiatique dont ils tirent parti et à des personnages comme Zemmour qui rendent leurs idées largement entendable voir "acceptable" par certain-es (on croit rêver)
- **Les droitards néolibéraux** rassemblé autour de Macron. Alors eux c'est les pragmatiques et les réalistes. Ceux qui savent comment on gouverne et qui même sans programme concret, sont élus. C'est sérieux ça ?

Effet intéressant de ces législatives, c'est qu'aucun bloc n'est majoritaire à l'assemblée nationale. Le président doit déjà regretter ces 5 dernières années où il pouvait s'en donner à coeur joie, maintenant il va devoir faire des compromis. Maintenant, il reste à voir lesquels, c'est là que ça devient inquiétant.

### L'extrême droite plus républicaine que la gauche ?

Depuis quelques jours, on entend partout dans les médias que certains Macroniste (et des ministres !) 


- Une majorité relative, du jamais vu. 3 blocs a l'Assemblée qui fait que c'est bloqué. 
- Explosion du barrage républicain, mise en équivalence de la gauche et l'extrême droite, différence entre les 50 défaites NUPES / RN de voix, le système de vote des législatives... 
- Le virage a droite de Macron : quand les députés commencent a sous entendre qu'un rapprochement avec Lepen serait envisageable

