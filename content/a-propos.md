---
title: A propos 
subtitle: Mais qui suis-je ? 
comments: false
---

![Montagne](/img/montagne.jpg)

La voix de Nadroj, c'est un blog dans lequel je partage mes réflexions et mes pensées sur plein de sujets, souvent avec un (petit) fond politique. 
